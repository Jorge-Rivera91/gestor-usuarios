import './style.css';

const Input = ({label, ...rest}) => {
  return (
    <div className="form-input">
      <label>{label}</label>
      <input {...rest} />
    </div>
  );
}

export default Input;