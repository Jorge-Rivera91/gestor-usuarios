import useFormulario from "../../hooks/useFormulario";
import Button from "../Button";
import Input from "../Input";

const initialState = {
  name: '',
  lastname: '',
  email: '',
};

const UserForm = ({submit}) => {
  const [formulario, handleChange, reset] = useFormulario(initialState);
  
  const handleSubmit = (e) => {
    e.preventDefault();
    submit(formulario);
    reset();
  }

  return (
    <form onSubmit={handleSubmit}>
    <Input
      label='Nombre'
      placeholder='Nombre'
      type='text'
      name='name'
      value={formulario.name}
      onChange={handleChange}
    />
    <Input
      label='Apellido'
      placeholder='Apellido'
      type='text'
      name='lastname'
      value={formulario.lastname}
      onChange={handleChange}
    />
    <Input
      label='Correo'
      placeholder='Correo'
      type='email'
      name='email'
      value={formulario.email}
      onChange={handleChange}
    />
    <Button>Enviar</Button>
  </form>
  );

}

export default UserForm;