import { useState } from "react";

const useFormulario = (initialState) => {
  const [fields, setFields] = useState(initialState);

  const handleInputs = (e) => {
    setFields({
      ...fields,
      [e.target.name]: e.target.value,
    });
  }

  const reset = () => {
    setFields(initialState);
  }

  return [fields, handleInputs, reset];
}

export default useFormulario;