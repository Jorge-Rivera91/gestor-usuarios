import { useState } from "react";
import Card from "./components/Card";
import Container from "./components/Container";
import UserForm from "./components/UserForm";


const App = () => {
  const [users, setUsers] = useState([]);
  
  const handleSubmit = (user) => {
    setUsers(users => [...users, user]);
  }

  return (
    <div style={{marginTop: '10%'}}>
      <Container>
        <Card>
          <UserForm submit={handleSubmit} />
        </Card>
        { users.length > 0 && (
          <Card>
            <ul>
              {users.map(user => (
                <li key={user.email} >{`${user.name} ${user.lastname} => ${user.email}`}</li>
              ))}
            </ul>
          </Card>
        )}
      </Container>
    </div>
  );
}

export default App;
